// --------
// RMSE.c++
// --------

#include <algorithm>  // transform
#include <cassert>    // assert
#include <cmath>      // sqrt
#include <functional> // plus
#include <iostream>   // cout, endl
#include <iterator>   // distance
#include <numeric>    // accumulate, transform_reduce
#include <vector>     // vector

using namespace std;

template <typename II1, typename II2, typename T>
T rmse1 (II1 b, II1 e, II2 c, T v) {
    if (b == e)
        return v;
    const auto s   = distance(b, e);
    const auto sqd = [] (const T& a, const T& p) -> T {const T d = a - p; return d * d;};
    vector<T>  x(s);
    transform(b, e, c, begin(x), sqd);
    v = accumulate(begin(x), end(x), v);
    return sqrt(v / s);}

template <typename II1, typename II2, typename T>
T rmse (II1 b, II1 e, II2 c, T v) {
    if (b == e)
        return v;
    const auto s   = distance(b, e);
    const auto sqd = [] (const T& a, const T& p) -> T {const T d = a - p; return d * d;};
    v = transform_reduce(b, e, c, v, plus<T>(), sqd);
    return sqrt(v / s);}

void test0 () {
    const vector<int> x = {2, 3, 4};
    const vector<int> y = {2, 3, 4};
	assert(rmse(begin(x), end(x), begin(y), 0.0) == 0);}

void test1 () {
    const vector<int> x = {2, 3, 4};
    const vector<int> y = {3, 2, 5};
	assert(rmse(begin(x), end(x), begin(y), 0.0) == 1);}

void test2 () {
    const vector<int> x = {2, 3, 4};
    const vector<int> y = {4, 1, 6};
	assert(rmse(begin(x), end(x), begin(y), 0.0) == 2);}

void test3 () {
    const vector<int> x = {1, 2, 3, 4, 5};
    const vector<int> y = {2, 5, 3, 1, 4};
	assert(rmse(begin(x), end(x), begin(y), 0.0) == 2);}

int main () {
    cout << "RMSE.c++" << endl;
    test0();
    test1();
    test2();
    test3();
    cout << "Done." << endl;
    return 0;}

/*
g++-9 -pedantic -std=c++17 -O2 -Wall -Wextra -Weffc++ Test.c++ -o Test
Test.c++: In instantiation of 'T rmse(II1, II1, II2, T) [with II1 = __gnu_cxx::__normal_iterator<const int*, std::vector<int> >; II2 = __gnu_cxx::__normal_iterator<const int*, std::vector<int> >; T = double]':
Test.c++:39:2:   required from here
Test.c++:33:25: error: no matching function for call to 'transform_reduce(__gnu_cxx::__normal_iterator<const int*, std::vector<int> >&, __gnu_cxx::__normal_iterator<const int*, std::vector<int> >&, __gnu_cxx::__normal_iterator<const int*, std::vector<int> >&, double&, std::plus<double>, const rmse(II1, II1, II2, T) [with II1 = __gnu_cxx::__normal_iterator<const int*, std::vector<int> >; II2 = __gnu_cxx::__normal_iterator<const int*, std::vector<int> >; T = double]::<lambda(const double&, const double&)>&)'
   33 |     v = transform_reduce(b, e, c, v, plus<T>(), sqd);
      |         ~~~~~~~~~~~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~
In file included from /usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/numeric:214,
                 from Test.c++:11:
/usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/pstl/glue_numeric_defs.h:35:1: note: candidate: 'template<class _ExecutionPolicy, class _ForwardIterator1, class _ForwardIterator2, class _Tp> __pstl::__internal::__enable_if_execution_policy<_ExecutionPolicy, _ForwardIterator2> std::transform_reduce(_ExecutionPolicy&&, _ForwardIterator1, _ForwardIterator1, _ForwardIterator2, _Tp)'
   35 | transform_reduce(_ExecutionPolicy&& __exec, _ForwardIterator1 __first1, _ForwardIterator1 __last1,
      | ^~~~~~~~~~~~~~~~
/usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/pstl/glue_numeric_defs.h:35:1: note:   template argument deduction/substitution failed:
Test.c++:33:25: note:   candidate expects 5 arguments, 6 provided
   33 |     v = transform_reduce(b, e, c, v, plus<T>(), sqd);
      |         ~~~~~~~~~~~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~
In file included from /usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/numeric:214,
                 from Test.c++:11:
/usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/pstl/glue_numeric_defs.h:41:1: note: candidate: 'template<class _ExecutionPolicy, class _ForwardIterator1, class _ForwardIterator2, class _Tp, class _BinaryOperation1, class _BinaryOperation2> __pstl::__internal::__enable_if_execution_policy<_ExecutionPolicy, _ForwardIterator2> std::transform_reduce(_ExecutionPolicy&&, _ForwardIterator1, _ForwardIterator1, _ForwardIterator2, _Tp, _BinaryOperation1, _BinaryOperation2)'
   41 | transform_reduce(_ExecutionPolicy&& __exec, _ForwardIterator1 __first1, _ForwardIterator1 __last1,
      | ^~~~~~~~~~~~~~~~
/usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/pstl/glue_numeric_defs.h:41:1: note:   template argument deduction/substitution failed:
Test.c++:33:25: note:   candidate expects 7 arguments, 6 provided
   33 |     v = transform_reduce(b, e, c, v, plus<T>(), sqd);
      |         ~~~~~~~~~~~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~
In file included from /usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/numeric:214,
                 from Test.c++:11:
/usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/pstl/glue_numeric_defs.h:47:1: note: candidate: 'template<class _ExecutionPolicy, class _ForwardIterator, class _Tp, class _BinaryOperation, class _UnaryOperation> __pstl::__internal::__enable_if_execution_policy<_ExecutionPolicy, _ForwardIterator2> std::transform_reduce(_ExecutionPolicy&&, _ForwardIterator, _ForwardIterator, _Tp, _BinaryOperation, _UnaryOperation)'
   47 | transform_reduce(_ExecutionPolicy&& __exec, _ForwardIterator __first, _ForwardIterator __last, _Tp __init,
      | ^~~~~~~~~~~~~~~~
/usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/pstl/glue_numeric_defs.h:47:1: note:   template argument deduction/substitution failed:
In file included from /usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/pstl/glue_algorithm_defs.h:15,
                 from /usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/algorithm:71,
                 from Test.c++:5:
/usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/pstl/execution_defs.h: In substitution of 'template<class ExecPolicy, class T> using __enable_if_execution_policy = typename std::enable_if<__pstl::execution::v1::is_execution_policy<typename std::decay<_Tp>::type>::value, T>::type [with ExecPolicy = __gnu_cxx::__normal_iterator<const int*, std::vector<int> >&; T = double]':
/usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/pstl/glue_numeric_defs.h:47:1:   required by substitution of 'template<class _ExecutionPolicy, class _ForwardIterator, class _Tp, class _BinaryOperation, class _UnaryOperation> __pstl::__internal::__enable_if_execution_policy<_ExecutionPolicy, _ForwardIterator2> std::transform_reduce(_ExecutionPolicy&&, _ForwardIterator, _ForwardIterator, _Tp, _BinaryOperation, _UnaryOperation) [with _ExecutionPolicy = __gnu_cxx::__normal_iterator<const int*, std::vector<int> >&; _ForwardIterator = __gnu_cxx::__normal_iterator<const int*, std::vector<int> >; _Tp = double; _BinaryOperation = std::plus<double>; _UnaryOperation = rmse(II1, II1, II2, T) [with II1 = __gnu_cxx::__normal_iterator<const int*, std::vector<int> >; II2 = __gnu_cxx::__normal_iterator<const int*, std::vector<int> >; T = double]::<lambda(const double&, const double&)>]'
Test.c++:33:25:   required from 'T rmse(II1, II1, II2, T) [with II1 = __gnu_cxx::__normal_iterator<const int*, std::vector<int> >; II2 = __gnu_cxx::__normal_iterator<const int*, std::vector<int> >; T = double]'
Test.c++:39:2:   required from here
/usr/local/Cellar/gcc/9.1.0/include/c++/9.1.0/pstl/execution_defs.h:155:7: error: no type named 'type' in 'struct std::enable_if<false, double>'
  155 | using __enable_if_execution_policy =
      |       ^~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
